1. Get a net install ISO from fedora and create a boot media.
2. Boot with the media.
3. Start rescue mode and go to shell instead of rescue shell, start optionally sshd, install packages
```
cp -p /etc/ssh/sshd_config.anaconda /etc/ssh/sshd_config
systemctl start sshd
echo "install_weak_deps=False" >> /etc/dnf/dnf.conf
echo "fastestmirror=True" >> /etc/dnf/dnf.conf
```

4. Partition a disk.
```
parted -a opt /dev/sda
mklabel gpt
unit mib
mkpart primary fat32 2 514
set 1 esp on
mkpart primary btrfs 514 -1
quit
```
5. Format the partitions
```
mkfs.vfat -F32 /dev/sda1
mkfs.btrfs /dev/sda2
```

6. Mount the partitions.
```
export sysroot=/mnt
export devname=sda
mount -o rw,noatime,discard /dev/${devname}2 $sysroot
mkdir -p $sysroot/boot
mount /dev/${devname}1 $sysroot/boot
```

7. Set up repositories and install dnf on the root.
```
mkdir -p /etc/yum.repos.d
cat > /etc/yum.repos.d/fedora.repo <<"EOF"
[fedora]
name=Fedora $releasever - $basearch
#baseurl=http://download.example/pub/fedora/linux/releases/$releasever/Everything/$basearch/os/
metalink=https://mirrors.fedoraproject.org/metalink?repo=fedora-$releasever&arch=$basearch
enabled=1
metadata_expire=7d
repo_gpgcheck=0
type=rpm
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
skip_if_unavailable=False
EOF
```

```
dnf install -y --installroot=$sysroot --releasever=40 dnf
```

8. chroot and install necessary packages for the installation
```
cp /etc/dnf/dnf.conf $sysroot/etc/dnf/
blkid
mount
mkdir -p $sysroot/etc
vi $sysroot/etc/fstab
UUID=4DC5-B30E /boot vfat rw,noatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,errors=remount-ro 0 0
UUID=fb587799-d57d-4f3c-8476-0318c5aba1d2 / btrfs rw,noatime,compress=zstd:1,ssd_spread,space_cache=v2,subvolid=5,subvol=/ 0 0
UUID=b55558c7-91e1-4978-aa3a-eb7df73741ef /media/1tbhdd btrfs user,nofail,rw,noatime,compress=zstd:10,space_cache=v2,subvolid=5,subvol=/ 0 0
systemd-nspawn -D $sysroot
mkdir -p /etc/dracut.conf.d
echo 'compress="zstd"' > /etc/dracut.conf.d/custom.conf
```

10. Install Kernel and Bootloader
```
dnf install --disablerepo=updates -y vim-minimal zstd btrfs-progs passwd systemd systemd-udev dracut dracut-config-rescue kernel linux-firmware systemd-networkd systemd-resolved
bootctl install
```

11. Install kernel to boot.
```
ker=`ls /usr/lib/modules`
kernel-install add $ker /lib/modules/$ker/vmlinuz
# set root for bootloader in options.
vi /boot/loader/entries/62c149c3e9934e4780abdfffe48eea41-5.5.17-200.fc31.x86_64.conf
options root=UUID=f4e476ad-0abe-4872-843a-b27ddfe8b849 rw quiet mitigations=off systemd.show_status=0 intel_iommu=off audit=0 selinux=0
```

12. Set root password, and add admin user
```
passwd
```

13. Some optional basic configurations
```
systemctl enable systemd-networkd
systemctl enable systemd-resolved
systemctl enable systemd-timesyncd
cat >/etc/systemd/network/dhcp.network <<"EOF"
[Match]
Name=enp*
[Network]
DHCP=ipv4
EOF
vi /etc/systemd/timesyncd.conf
```

15. configure systemd reosolved
```
ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
```

14. Post installation.
```
# Customisation for desktop use
dnf install -y dnf-plugins-core fedora-workstation-repositories
dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
dnf copr enable kohnish/main
<!-- dnf config-manager --set-enabled google-chrome -->
<!-- dnf config-manager --add-repo https://repo.vivaldi.com/archive/vivaldi-fedora.repo -->

rpm --import https://packages.microsoft.com/keys/microsoft.asc
dnf config-manager --add-repo https://packages.microsoft.com/yumrepos/edge
dnf update --refresh --disablerepo=updates
dnf install --disablerepo=updates -y emacs-lucid vi clang-tools-extra tmux novnc tigervnc-server zsh git less sudo alsa-utils xdotool p7zip p7zip-plugins celluloid hostname tar openssh-server openssh-clients iproute iputils bind-utils git-credential-libsecret bspwm parcellite bluez rofi-vanilla polybar xorg-x11-server-Xorg xorg-x11-xinit redshift feh gnome-keyring polkit-gnome playerctl pulseaudio pulseaudio-utils pulseaudio-module-bluetooth pcmanfm-qt ffmpeg keepassxc microsoft-edge-stable dnsmasq strongswan-vanilla fcitx5-configtool fcitx5 fcitx5-mozc

<!-- sway copyq wl-clipboard wpaste wlsunset waybar fuse vivaldi-stable keepassxc xclip -->

# Checkout git bare repo for root configs
git clone --bare https://gitlab.com/kohnish/root.git $HOME/.home.git
git --git-dir=$HOME/.home.git --work-tree=/ checkout
git --git-dir=$HOME/.home.git --work-tree=/ config --local status.showUntrackedFiles no
# Checkout git bare repo for home configs
su - kohnish
git clone --bare https://gitlab.com/kohnish/home.git $HOME/.home.git
git --git-dir=$HOME/.home.git --work-tree=$HOME checkout
git --git-dir=$HOME/.home.git --work-tree=$HOME config --local status.showUntrackedFiles no
```

13. Exit chroot and reboot.
```
exit
umount $sysroot
reboot
```

14. Post installation.
```
# system setting
hostnamectl set-hostname myfedora
localectl set-locale LANG=en_US.UTF8
timedatectl set-timezone Europe/Amsterdam
timedatectl set-ntp true
reboot
```


Notes:
```
Startup finished in 5.320s (firmware) + 301ms (loader) + 1.597s (kernel) + 1.383s (initrd) + 3.743s (userspace) = 12.345s
graphical.target reached after 3.382s in userspace
Startup finished in 5.504s (firmware) + 283ms (loader) + 1.491s (kernel) + 1.766s (initrd) + 2.784s (userspace) = 11.830s
graphical.target reached after 2.686s in userspace
Startup finished in 5.549s (firmware) + 286ms (loader) + 1.525s (kernel) + 1.742s (initrd) + 10.331s (userspace) = 19.435s
graphical.target reached after 1.725s in userspace
2040 can sails through arctic. no ice.
```
